import React, { Component } from 'react'
import './Item.css';
export default class
    extends Component {
    render() {
        return (
            
                <div className="card bg-light border-0 h-100">
                    <div className="card-body text-center p-4 pt-0 pt-lg-0">
                        <div className="icon bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                            <i class="fa fa-file-code"></i>
                        </div>
                        <h2 class="fs-4 fw-bold">Simple clean code</h2>
                        <p class="mb-0">We keep our dependencies up to date and squash bugs as they come!</p>
                    </div>
                </div>
           
        )
    }
}
