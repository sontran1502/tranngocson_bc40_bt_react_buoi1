import React, { Component } from 'react'
import Content from './Content'
import Footer from './Footer'
import Header from './Header'
import Item from './Item'

export default class Playout extends Component {
  render() {
    return (
      <div>
        <Header/>
        <div className="py-5">
            <div className="container ">
            <Content/>
            </div>
        </div>
        <div className="pt-4">
        <div className="container px-lg-5">
        <div className="row gx-lg-5">
          <div className="col-lg-6 col-xl-4 mb-5">
            <Item/>
          </div>
          <div className="col-lg-6 col-xl-4 mb-5">
            <Item/>
          </div>
          <div className="col-lg-6 col-xl-4 mb-5">
            <Item/>
          </div>
          <div className="col-lg-6 col-xl-4 mb-5">
            <Item/>
          </div>
          <div className="col-lg-6 col-xl-4 mb-5">
            <Item/>
          </div>
          <div className="col-lg-6 col-xl-4 mb-5">
            <Item/>
          </div>
        
        </div>
        </div>
        </div>
        <div className="">
          <Footer/>
        </div>
      </div>
    )
  }
}
