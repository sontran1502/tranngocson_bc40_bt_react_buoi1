import logo from './logo.svg';
import './App.css';
import Playout from './Playout/Playout';

function App() {
  return (
    <div className="App">
     <Playout/>
    </div>
  );
}

export default App;
